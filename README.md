# phage_isolation_to_bioinformatic_approach

You can find here the scripts corresponding to this paper: "A simple, reproducible and cost-effective procedure to analyse gut phageome: from phage isolation to bioinformatic approach"

## Assembly

You can find here the scripts
- assembly_pipeline_cdhumieres.sh: to clean and assemble reads
- Assembly_to_Matrix_count.sh: to select contigs with a minimum of 3 ORF, to remove redundancy between contigs from different samples and to map reads on these contigs

## wGRR

You can find here the scripts to perform the weighted gene repertoire relatedness (wGRR) of the contigs and different databases:
- 1943 phages from Refseq database (GenBank Refseq (ftp://ftp.ncbi.nih.gov/genomes; last accessed November 2016)
- 456 putative prophages in the draft genomes of the Human Microbiome Project (HMP) (http://hmpdacc.org/HMRGD, last accessed in December 2015).
- 6426 putative prophages from complete bacterial genomes from GenBank Refseq



## Contig exploration

You can find here the scripts corresponding to the exploration and the classification of the contigs

## Figure

A notebook python with the code for the 5 main figures of the article













