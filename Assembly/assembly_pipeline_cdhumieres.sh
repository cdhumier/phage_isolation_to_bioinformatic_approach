#!/bin/sh


###########################Command line###########################


#assembly_pipeline_cdhumieres.sh -1 XXX_R1.fastq -2 XXX_R2.fastq -s orf -m 1 -d 1 -t 60 -a spades -c 10 -l 100 -r 35
#-s until ORF step
#-m 1 metagenomic data
# -d 1 remove incomplete proteins
#-t 60 cut off proteins size: 60aa
#-a spades SPADES assembler
#-r 50 ram
#-c 10 number of CORE
#-l 100 cut off reads size
##################################################################

source ~/.dependencies


# read the options


# extract options and their arguments into variables.
if [[ $# -eq 0 ]] ; then
echo "No variables given."
echo "Usage: $0 -1 <R1 file> -2 <R2 file> -s <Stop position> -m <Metagenome 1|0>"
exit 0
fi
while getopts "h:1:2:s:m:c:r:a:k:o:d:t:l:" arg; do
case $arg in
1) file1=$OPTARG ;;
2) file2=$OPTARG ;;
s) stop=$OPTARG ;;
a) assembly=$OPTARG ;;
m) meta=$OPTARG ;;
c) cores=$OPTARG ;;
r) ram=$OPTARG ;;
d) double_ends=$OPTARG ;;
t) size_threshold=$OPTARG ;;
k) kmers=$OPTARG ;; #doesn't do anything now
l) len=$OPTARG ;;
*)
echo "usage $0 <OPTIONS> -1 <R1 file> -2 <R2 file>"
echo "	-1 R1 file"
echo "	-2 R2 file"
echo "	-s Step at which the pipeline will stop"
echo "	-m <0|1> Is Metagenomic data? <Default = 0>"
echo "	-a Assembly Method <spades|minia>. Default = Spades"
echo "	-c Number of cores. Default: Half of the cores in the computer"
echo "	-r RAM to be used (in Gb). Default: Half of the total RAM"
echo "	-d <0|1> remove the double incomplete? 1=YES 0=NO. <Default = 0>"
echo "	-t Size threshold of ORFs to be kept to the annotation. <Default  = NA>";
echo "	-k <String>; K-mers to be used during the assembly" # Now it doesn't yet work
echo "	-l <Num>; Minimum length for UrQt"
exit 1;
;;
esac
done


#kill the whole thing if the input files are not defined
if [ -z $file1 ]; then
echo "Input files are not defined. Aborting"
exit 1;
elif [ -z $file2 ]; then
echo "Input files are not defined. Aborting"
exit 1;
fi
#Definition of working directory
wd=$(dirname $0)
#Definition of cores. if not defined, I ask for 1/2 of the total amount of cores
if [ -z $cores ]; then
tmp=$(sysctl -a | grep machdep.cpu.thread_count | awk '{print $2}')
cores=$(($tmp/2))
fi
#Definition of RAM. if not defined I ask for 1/2 of the total amount of RAM
if [ -z $ram ]; then
tmp=$(sysctl -a | grep memsize | awk '{print $2}')
#I divide by the num of bytes in a Gigabyte
tmp=$(($tmp/1073741824))
ram=$(($tmp/2))
fi
echo I am going to use $ram Gb of RAM.



# I start the pipeline
current=$(pwd) # definition of the working directory and the current

#I check whether the file exists and whether it exists in current location
if [ -e $file1 ]; then
filename1=$(basename $file1)
filename1=${filename1%.*}  #enlever le .fastq
#to generate a name to use for the logs
filename=${filename1%R1*}
char2rm=${filename: -1} #"_" le separateur
filename=${filename%$char2rm}
direname=$(echo $filename1 | cut -d $char2rm -f1,2,3); #j'ai mis -f1,2,3 pour prendre tout ce qui est avant le R1/R2, cf mon nouveau formatage pour les patients CEREMI
elif [ -e $current/$file1 ]; then
file1=$(echo $current"/"$file1)
tmp=${file1%%.*}
filename=$(basename $tmp)
direname=$(echo $filename1 | cut -d "_" -f1,2,3);
else
echo "$file1: Unknown Input file or directory. Aborting"
exit 1;
fi
ends="2"
#I look now for the R2 file
if [ -e $file2 ]; then
filename2=$(basename $file2)
filename2=${filename2%.*}
elif [ -e $current/$file2 ]; then
file2=$(echo $current"/"$file2)
tmp=${file2%%.*}
filename2=$(basename $tmp)
else
echo "$file2: Unknown Input file or directory. Aborting"
exit 1;
fi
echo $out
if [ ! -z $out ]; then
direname=$out
echo $direname
fi
#Defining workspace
echo "I will work with $file1 and $file2"
#chooses type of pipeline he's going to use (depending on metagenomics or not)
if [[ $meta == "0" ]]; then
echo "I will perform a careful assembly of $filename"
elif [[ $meta == "1" ]]; then
echo "I will perform a metagenomic assembly of $filename"
else
meta=1
fi

#definition of other variables

#remove trimmed sequences by threshold
if [ -z $len ]; then
len=80
echo "I will remove trimmed reads of size smaller than 80"
else
echo "I will remove trimmed reads of size smaller than $len"
fi

#double incomplete
if [ -z $double_ends ]; then
double_ends=0
echo "I will keep all predicted ORFs, even if they have incomplete ends"
else
echo "I will remove ORFs with incomplete ends in both sides"
fi

#size threshold
if [ -z $size_threshold ]; then
size_threshold=0
echo "I will keep ORFs of all sizes"
else
echo "I will remove ORFs of size smaller than $size_threshold"
fi

#Uncompressing if files are compressed
if [[ $file1 =~ ".gz" ]] ; then
gunzip $file1
gunzip $file2
file1=${file1%.*}
file2=${file2%.*}
fi

#Phase I. Cleaning the data
#I define the working directory and write the output (now the output directory will placed
#inside of the input folder

directory=$(dirname $file1)
if [ ! -d "$directory/processed" ] ; then
mkdir $directory/processed/
fi
#I select the last name of direname
if [[ $direname == */* ]]; then  direname=$(basename $direname); fi


dt_beg=$(date)
dt_beg_s=$(date +%s)
echo "starting to process the sequencing project: $filename at $dt_beg";

if [ ! -d "$directory/processed/$direname" ] ; then
mkdir -p $directory/processed/$direname
fi

out1=$directory/processed/$direname/$filename1.cutadapt.fastq
out2=$directory/processed/$direname/$filename2.cutadapt.fastq

echo "       Adaptor Trimming"
if [ ! -e "$directory/processed/$direname/cutadapt.log" ]; then
#I have given the Illumina adaptors. This can be changed in newer versions to make it work with other platforms
cutadapt -a AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT -A AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT -o $out1 -p $out2 $file1 $file2 > $directory/processed/$direname/cutadapt.log
fi
file1=$out1
file2=$out2
if [ $stop = 'cutadapt' ]; then
dt_end=$(date)
dt_end_s=$(date +%s)
dt_df=$(($dt_end_s - $dt_beg_s))
min=$(($dt_df / 60))
sec=$(($dt_df % 60))
echo "Done! at $dt_end"
echo "Time lapse: $min minutes and $sec seconds";
exit
fi
#UrQt
sleep 5; #I make it sleep 5sec so RAM can be reubicated.
#Sometimes, in small machines UrQt crashes when it tries to reallocate the memory
#With this 5sec, it never crashes in my machine.
echo "       Quality Trimming"
out1=${file1/.cutadapt.fastq/.ctd.urqt.fastq}
out2=${file2/.cutadapt.fastq/.ctd.urqt.fastq}

if [ ! -e "$directory/processed/$direname/urqt.log" ]; then
#UrQt calling. Check why sometimes crashes
UrQt --in $file1 --inpair $file2 --out $out1 --outpair $out2 --pos both --min_QC_length 80 --min_read_size $len --min_QC_phred 20 --m 12 >> $directory/processed/$direname/urqt.log 2>> $directory/processed/$direname/urqt.log
#attention erreur de marc j'ai fixé le --min_QC_length 80 et j'ai rajouté le --min_read_size $len pour fixer ma taille de read minimum (par default = 15)
fi
#control whether UrQt has worked
out1size=$(du $out1 | awk '{print $1}')
out2size=$(du $out2 | awk '{print $2}')
if [[ $out1size = "0" ]]; then
echo "UrQt failed before finishing. Aborting"
rm $directory/processed/$direname/urqt.log
rm $out1
if [ -e $out1.tmp ]; then rm $out1.tmp; fi
rm $out2
if [ -e $out2.tmp ]; then rm $out2.tmp; fi
exit
elif [[ $out2size = "0" ]]; then
echo "UrQt failed before finishing. Aborting"
rm $directory/processed/$direname/urqt.log
rm $out1
if [ -e $out1.tmp ]; then rm $out1.tmp; fi
rm $out2
if [ -e $out2.tmp ]; then rm $out2.tmp; fi
exit
fi

if [ $stop = 'urqt' ]; then
dt_end=$(date)
dt_end_s=$(date +%s)
dt_df=$(($dt_end_s - $dt_beg_s))
min=$(($dt_df / 60))
sec=$(($dt_df % 60))
echo "Done! at $dt_end"
echo "Time lapse: $min minutes and $sec seconds";
exit
fi
####End of cleaning part. Starting with Assembly

file1=$out1
file2=$out2

echo "       Assembly"
#chooses the assembler
sleep 5; #I make it sleep 5sec so RAM can be reubicated.


if [ $assembly = 'spades' ]; then
#with SPADES and KHMER

#khmer
if [ ! -e "$directory/processed/$direname/$filename.dn.R1.fastq" ]; then

/Users/camilledhumieres/Documents/Logiciels/khmer-2.0/scripts/interleave-reads.py $file1 $file2 --output $directory/processed/$direname/$filename.pe
/Users/camilledhumieres/Documents/Logiciels/khmer-2.0/scripts/normalize-by-median.py --paired --ksize 20 --cutoff 20  --max-tablesize 3e9 --savegraph graph.ct $directory/processed/$direname/$filename.pe --output $directory/processed/$direname/$filename.pe.keep
/Users/camilledhumieres/Documents/Logiciels/khmer-2.0/scripts/filter-abund.py -V graph.ct $directory/processed/$direname/$filename.pe.keep --output $directory/processed/$direname/$filename.pe.filtered
/Users/camilledhumieres/Documents/Logiciels/khmer-2.0/scripts/extract-paired-reads.py $directory/processed/$direname/$filename.pe.filtered --output-paired $directory/processed/$direname/$filename.dn.pe --output-single $directory/processed/$direname/$filename.dn.se
/Users/camilledhumieres/Documents/Logiciels/khmer-2.0/scripts/split-paired-reads.py $directory/processed/$direname/$filename.dn.pe -1 $directory/processed/$direname/$filename.dn.R1.fastq -2 $directory/processed/$direname/$filename.dn.R2.fastq


fi

#SPAdes genome assembler v3.10.1 (changer le 22 mai 2017) (avant v3.9.0)



if [ ! -e "$directory/processed/$direname/spades/contigs.fasta" ]; then
touch $directory/processed/$direname/assembly_w_spades.log
if [[ $meta == "0" ]]; then
spades.py -1 $directory/processed/$direname/$filename.dn.R1.fastq -2 $directory/processed/$direname/$filename.dn.R2.fastq -t $cores -m $ram --careful -o $directory/processed/$direname/spades >>$directory/processed/$filename.Assembly_w_spades.log 2>>$directory/processed/$filename.Assembly_w_spades.log
else

spades.py -1 $directory/processed/$direname/$filename.dn.R1.fastq -2 $directory/processed/$direname/$filename.dn.R2.fastq -t $cores -m $ram --meta -o $directory/processed/$direname/spades >>$directory/processed/$filename.Assembly_w_spades.log 2>>$directory/processed/$filename.Assembly_w_spades.log
fi
fi
contigs=$directory/processed/$direname/spades/contigs.fasta


elif [ $assembly = 'minia' ]; then
#with MINIA

if [ ! -e "$directory/processed/$direname/output_minia/output_minia.fasta" ];  then
touch $directory/processed/$direname/assembly_w_minia.log
if [[ $meta == "0" ]]; then
echo "	Starting Minia for genomes"
ram=$(($ram*1000))
echo "You have stated that Minia will use $ram Mb of RAM. Be sure you have enough"
$wd/gatb -1 $file1 -2 $file2 -o output_minia --nb_cores $cores --max-memory $ram >> $directory/processed/$direname/assembly_w_minia.log 2>>$directory/processed/$direname/assembly_w_minia.log
mkdir $directory/processed/$direname/output_minia/
mv output_minia* $directory/processed/$direname/output_minia/
else
echo "	Starting Minia for metagenomes"
ram=$(($ram*1000))
echo "You have stated that Minia will use $ram Mb of RAM. Be sure you have enough"
touch "$directory/processed/$direname/file.lst"
echo $file1 > $directory/processed/$direname/file.lst
echo $file2 >> $directory/processed/$direname/file.lst
$wd/gatb -l $directory/processed/$direname/file.lst -o output_minia --nb_cores $cores --max-memory $ram > $directory/processed/$direname/assembly_w_minia.log 2>$directory/processed/$direname/assembly_w_minia.log

echo "j'ai fini minia et je cree le dossier output_Minia"

mkdir $directory/processed/$direname/output_minia/
mv output_minia* $directory/processed/$direname/output_minia/
fi
if [ -d BESST_tmp ]; then
rm -r BESST_tmp
fi
rm -r trashme*
fi
contigs=$directory/processed/$direname/output_minia/output_minia.fasta
if [ ! -e $contigs ]; then
echo "Assembly failed. Please check the assembly logfile. Aborting."
exit
fi
fi

if [ $stop = 'assembly' ]; then
dt_end=$(date)
dt_end_s=$(date +%s)
dt_df=$(($dt_end_s - $dt_beg_s))
min=$(($dt_df / 60))
sec=$(($dt_df % 60))
echo "Done! at $dt_end"
echo "Time lapse: $min minutes and $sec seconds";
exit
fi

####END of the assembly. Start of the ORF calling

#PROKKA

echo '       ORF search'
if [[ $meta == "0" ]]; then
if [ ! -d "$directory/processed/$direname/ORF/" ] ; then
$spades_rename $contigs
contigs=${contigs/\.fasta/\.renamed.fasta}
touch $directory/processed/$direname/PROKKA.log
prokka --outdir $directory/processed/$direname/ORF/ --kingdom Bacteria --gcode 11 $contigs >> $directory/processed/$direname/PROKKA.log 2>>$directory/processed/$direname/PROKKA.log
for prokka in $directory/processed/$direname/ORF/* ; do
prokkaname=$(basename $prokka)
extension=${prokkaname##*.}
mv $prokka $directory/processed/$direname/PROKKA/$direname.$extension
done
fi
else
if [ ! -d "$directory/processed/$direname/ORF/" ] ; then
prodigal_parser="$wd/master_scripts/Do_Format_prodigal.py"
if [ ! -e $prodigal_parser ]; then
echo "aborting"
else

fileout=${directory}/processed/$direname/spades/${direname}.contig.format.fasta
if [ ! -e $fileout ]
then
awk -v name=$direname -F "_" '{if(/^>/) printf ">%s_%.7d_%d_%.2f\n", name,$2,$4,$6; else print $0}' ${directory}/processed/$direname/spades/contigs.fasta > $fileout
fi


touch $directory/processed/$direname/prodigal.log
mkdir $directory/processed/$direname/ORF/

prodigal -i $fileout -a $directory/processed/$direname/ORF/$direname.prt -d $directory/processed/$direname/ORF/$direname.gen -o $directory/processed/$direname/ORF/$direname.prodigal.out -p meta  >>$directory/processed/$direname/prodigal.log 2>>$directory/processed/$direname/prodigal.log

echo "je vais formater la sortie de prodigal"

python $prodigal_parser $directory/processed/$direname/ORF/$direname.gen $directory/processed/$direname/ORF/$direname.prt $directory/processed/$direname/ORF/$direname.format.gen $directory/processed/$direname/ORF/$direname.format.prt $directory/processed/$direname/ORF/$direname.format.lst $double_ends



fi
fi
fi

#Le CUTT OFF de taille de prodigal est de 60 donc pas de filtre de taille


#Camille: je rajoute l'etape de creer un directory interest avec les contig (changer le header des contigs + renommer le fichier) et les genes + prot
## pour spades ##

if [ ! -d $directory/processed/$direname/interest ]
then
mkdir $directory/processed/$direname/interest
fi



if [ -e $directory/processed/$direname/ORF/${direname}.format.prt ]
then

mv $directory/processed/$direname/ORF/${direname}.format.prt $directory/processed/$direname/interest

mv $directory/processed/$direname/ORF/${direname}.format.gen $directory/processed/$direname/interest

mv $directory/processed/$direname/ORF/${direname}.format.lst $directory/processed/$direname/interest

mv $directory/processed/$direname/spades/${direname}.contig.format.fasta $directory/processed/$direname/interest/${direname}.contig.fasta

fi

# deplacer les reads bruts trimés new name dans le directory Read_bruts_trimes_NewName

#if [  -e $directory/processed/$direname/${direname}_R1.ctd.urqt.fastq ]
#then

#DirectoryReadTrimes=/Volumes/BIGDATA/Echantillons_Temoins/Read_bruts_trimes

#mv $directory/processed/$direname/${direname}_R1.ctd.urqt.fastq $DirectoryReadTrimes
#mv $directory/processed/$direname/${direname}_R2.ctd.urqt.fastq $DirectoryReadTrimes


#fi

#enlever les fichier cutadapt + sortie de KHMER

rm $directory/processed/$direname/${direname}_R1.cutadapt.fastq
rm $directory/processed/$direname/${direname}_R2.cutadapt.fastq
rm $directory/processed/$direname/${direname}.dn*
rm $directory/processed/$direname/${direname}.pe*



#zipper spades puis effacer spades

zip -r $directory/processed/$direname/spades.zip $directory/processed/$direname/spades
rm -r $directory/processed/$direname/spades



if [ $stop = 'orf' ]; then
dt_end=$(date)
dt_end_s=$(date +%s)
dt_df=$(($dt_end_s - $dt_beg_s))
min=$(($dt_df / 60))
sec=$(($dt_df % 60))
echo "Done! at $dt_end"
echo "Time lapse: $min minutes and $sec seconds";
exit
fi




#Functional annotations (eggNOG, KEGG, Pfam)
#At this point I have decided to use the output of prokka. But I can use prodigal, too.
if [ ! -d "$directory/processed/$direname/Functional_Annotation" ]; then
mkdir $directory/processed/$direname/Functional_Annotation

fi
echo 'EggNOG annotation'
if [ ! -d "$directory/processed/$direname/Functional_Annotation/eggNOG" ] ; then
mkdir $directory/processed/$direname/Functional_Annotation/eggNOG/
hmmsearch -o /dev/null --cpu 12 --domtblout $directory/processed/$direname/Functional_Annotation/eggNOG/$direname.eggNOG.annotation.tblout $EGGNOG_DB $orf
fi
if [ ! -d "$directory/processed/$direname/Functional_Annotation/eggNOG/tables" ] ; then
perl /Volumes/biggem/Users/mgarcia/data/scripts/domtblout2table.pl -i $directory/processed/$direname/Functional_Annotation/eggNOG/$direname.eggNOG.annotation.tblout -o $directory/processed/$direname/Functional_Annotation/eggNOG/tables/
fi

if [ $stop = 'eggnog' ]; then
dt_end=$(date)
dt_end_s=$(date +%s)
dt_df=$(($dt_end_s - $dt_beg_s))
min=$(($dt_df / 60))
sec=$(($dt_df % 60))
echo "Done! at $dt_end"
echo "Time lapse: $min minutes and $sec seconds";
exit
fi

echo 'KEGG annotation'
if [ ! -d "$directory/processed/$direname/Functional_Annotation/KEGG" ] ; then
mkdir $directory/processed/$direname/Functional_Annotation/KEGG/
hmmsearch -o /dev/null --cpu 12 --domtblout $directory/processed/$direname/Functional_Annotation/KEGG/$direname.KEGG.annotation.tblout $KEGG_DB $directory/processed/$direname/ORF/$direname.faa
fi
if [ ! -d "$directory/processed/$direname/Functional_Annotation/KEGG/tables" ]; then
perl /Volumes/biggem/Users/mgarcia/data/scripts/domtblout2table.pl -i $directory/processed/$filename/Functional_Annotation/KEGG/$direname.KEGG.annotation.tblout -o $directory/processed/$direname/Functional_Annotation/KEGG/tables/
fi

if [ $stop = 'kegg' ]; then
dt_end=$(date)
dt_end_s=$(date +%s)
dt_df=$(($dt_end_s - $dt_beg_s))
min=$(($dt_df / 60))
sec=$(($dt_df % 60))
echo "Done! at $dt_end"
echo "Time lapse: $min minutes and $sec seconds";
exit
fi

echo 'Pfam annotation'
if [ ! -d "$directory/processed/$direname/Functional_Annotation/Pfam/" ] ; then
mkdir $directory/processed/$direname/Functional_Annotation/Pfam/
hmmsearch -o /dev/null --cpu 12 --domtblout $directory/processed/$direname/Functional_Annotation/Pfam/$direname.Pfam.annotation.tblout $PFAM_$DB $directory/processed/$direname/ORF/$direname.faa
fi
if [ ! -d "$directory/processed/$direname/Functional_Annotation/Pfam/tables" ]; then
perl /Volumes/biggem/Users/mgarcia/data/scripts/domtblout2table.pl -i $directory/processed/$direname/Functional_Annotation/Pfam/$direname.Pfam.annotation.tblout -o $directory/processed/$filename/Functional_Annotation/Pfam/tables/

fi

if [ $stop = 'pfam' ]; then
dt_end=$(date)
dt_end_s=$(date +%s)
dt_df=$(($dt_end_s - $dt_beg_s))
min=$(($dt_df / 60))
sec=$(($dt_df % 60))
echo "Done! at $dt_end"
echo "Time lapse: $min minutes and $sec seconds";
exit
fi

dt_end=$(date)
dt_end_s=$(date +%s)
dt_df=$(($dt_end_s - $dt_beg_s))
min=$(($dt_df / 60))
sec=$(($dt_df % 60))
echo "Done! at $dt_end"
echo "Time lapse: $min minutes and $sec seconds";
