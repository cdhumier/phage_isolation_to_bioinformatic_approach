#!/bin/bash


##########################PATH###########################
workdir=/Echantillons_Temoins/Read_bruts/processed
workdir2=/Echantillons_Temoins/Read_bruts/processed/Patient/ALL #je focusse sur ALL et non par patient
WorkDirBowtie=Echantillons_Temoins/Read_bruts_trimes #chemin des reads trimés
##########################################################

S

# mkdir

if [ ! -d $workdir/Patient ]
then
    mkdir $workdir/Patient
fi

# mkdir ALL

if [ ! -d $workdir/Patient/ALL ]
then
mkdir $workdir/Patient/ALL
fi

#mkdir for each sample

Patients=$(ls $workdir | awk -F "_" '{print substr($1,1,2)}'  | sort | uniq -c | awk '{ print $2}' | head  -3)  #j'ai un fichier OLD que je ne prend pas en compte grace au head -n 1
#AA BB CC

for p in $Patients
do
    if [ ! -d $workdir/Patient/$p ]
    then
    mkdir $workdir/Patient/${p}
    fi
done



Patients_ALL=$(ls $workdir/Patient/)


for patient in $Patients_ALL
do
    if [ ! -e $wordir/Patient/${patient}/${patient}.contig.fa ]
    then
        if [ $patient == "ALL" ]
        then

            cat $workdir/*/interest/*contig.fasta > ${workdir}/Patient/${patient}/${patient}.contig.fasta
        else
            cat $workdir/${patient}*/interest/*contig.fasta > ${workdir}/Patient/${patient}/${patient}.contig.fasta
        fi
    fi


    if [ ! -e $wordir/Patient/${patient}/${patient}.genes.gen ]
    then
        if [ $patient == "ALL" ]
        then cat $workdir/*/interest/*.filtered.gen > ${workdir}/Patient/${patient}/${patient}.genes.gen
        else
        cat $workdir/${patient}*/interest/*.filtered.gen > ${workdir}/Patient/${patient}/${patient}.genes.gen
        fi
    fi

    if [ ! -e $wordir/Patient/${patient}/${patient}.proteins.prt ]
    then
        if [ $patient == "ALL" ]
        then
            cat $workdir/*/interest/*.filtered.prt > ${workdir}/Patient/${patient}/${patient}.proteins.prt

            if [ ! -e Node_min2ORF.lst ]
            then
             grep ">" ${workdir}/Patient/${patient}/${patient}.proteins.prt  |  awk -F '_' '{print $1"_"$2"_"$3"_"$4}' | tr -d ">" | sort | uniq -c | awk '{if ($1 >= 2) print $2}' > ${workdir}/Patient/${patient}/Node_min2ORF.lst
             grep ">" ${workdir}/Patient/${patient}/${patient}.proteins.prt  |  awk -F '_' '{print $1"_"$2"_"$3"_"$4}' | tr -d ">" | sort | uniq -c | awk '{if ($1 >= 3) print $2}' > ${workdir}/Patient/${patient}/Node_min3ORF.lst
            grep ">" ${workdir}/Patient/${patient}/${patient}.proteins.prt  |  awk -F '_' '{print $1"_"$2"_"$3"_"$4}' | tr -d ">" | sort | uniq -c | awk '{if ($1 >= 4) print $2}' > ${workdir}/Patient/${patient}/Node_min4ORF.lst
            fi
        else
        cat $workdir/${patient}*/interest/*.filtered.prt > ${workdir}/Patient/${patient}/${patient}.proteins.prt
        fi
    fi


done



#select contigs with a minimum of 3 ORF
if [ ! -e $workdir2/Node_min3ORF.contig.fasta ]

then

python ContigLST_to_ContigSEQ.py


fi


####remove redondancy

for patient in $Patients_ALL
do
  if [ ! -e ${workdir}/Patient/${patient}/${patient}.genes.gen.id95_cov90 ]
  then
  cd-hit-est -i ${workdir}/Patient/${patient}/${patient}.genes.gen -o ${workdir}/Patient/${patient}/${patient}.genes.gen.id95_cov90 -M 0 -T 8 -d 0 -g 1 -c 0.95 -aS 0.9 > ${workdir}/Patient/${patient}/${patient}.genes.gen.log 2>${workdir}/Patient/${patient}/${patient}.genes.gen.err
  fi
  if [ ! -e ${workdir}/Patient/${patient}/${patient}.contig.fa.id95_cov90 ]
  then
  cd-hit-est -i ${workdir}/Patient/${patient}/${patient}.contig.fasta -o ${workdir}/Patient/${patient}/${patient}.contig.fa.id95_cov90 -M 0 -T 8 -d 0 -g 1 -c 0.95 -aS 0.9 > ${workdir}/Patient/${patient}/${patient}.contig.fa.log 2>${workdir}/Patient/${patient}/${patient}.contig.fa.err
  fi
done

#pour Node_min3ORF.contig.fa.id95_cov90 j'enleve la redondance que pour les contigs


if [ ! -e $workdir2/Node_min3ORF.contig.fa.id95_cov90 ]
then
echo "je fais CDHIT"
cd-hit-est -i $workdir2/Node_min3ORF.contig.fasta -o $workdir2/Node_min3ORF.contig.fa.id95_cov90 -M 0 -T 8 -d 0 -g 1 -c 0.95 -aS 0.9 > $workdir2/Node_min3ORF.contig.fa.log 2>$workdir2/Node_min3ORF.contig.fa.err

fi




if [ ! -e $workdir2/Node_min3ORF.contig.fa.id95_cov90.contig.lst ]
then
grep ">" $workdir2/Node_min3ORF.contig.fa.id95_cov90 | tr -d ">" > $workdir2/Node_min3ORF.contig.fa.id95_cov90.contig.lst
fi


if [ ! -e $workdir2/Node_min3ORF.contig.fa.id95_cov90.proteins.lst ]
then
    virome=$(awk -F"_" '{print $1"_"$2"_"$3}' $workdir2/Node_min3ORF.contig.fa.id95_cov90.contig.lst | sort | uniq) #je fais la liste des virome sur lequels je vais intervenir

    for vir in $virome

    do


    # je recapture la liste des numeros des contigs pour chaque virome

    Contig_Liste=$(grep ${vir} ${workdir2}/Node_min3ORF.contig.fa.id95_cov90.contig.lst | awk -F "_" '{print $4}' | sort | uniq) # je capture la liste des numeros du contigs par virome

    # pour chaque numero je vais récupérer le nom des différentes protéines

    for contig in $Contig_Liste
    do

    awk -v contig=$contig '{if ($7 == contig) print $1}' ${workdir}/${vir}/interest/${vir}.filtered.lst >> $workdir2/Node_min3ORF.contig.fa.id95_cov90.proteins.lst
    done

    done

    fi

#a present je dois recuperer la sequence GENIQUE et PROTEIQUE associé a Contig_to_proteins_orf3.lst

if [ ! -e $workdir2/Node_min3ORF.contig.fa.id95_cov90.gen ] # faire aussi le .gen

then

python Contig_to_proteins.py
fi




##Bowtie index
for patient in $Patients_ALL
do
  if [ ! -e ${workdir}/Patient/${patient}/${patient}.genes.gen.id95_cov90.1.bt2 ]
  then
  bowtie2-build ${workdir}/Patient/${patient}/${patient}.genes.gen.id95_cov90 ${workdir}/Patient/${patient}/${patient}.genes.gen.id95_cov90
  fi
  if [ ! -e ${workdir}/Patient/${patient}/${patient}.contig.fa.id95_cov90.1.bt2 ]
  then
  bowtie2-build ${workdir}/Patient/${patient}/${patient}.contig.fa.id95_cov90 ${workdir}/Patient/${patient}/${patient}.contig.fa.id95_cov90
  fi

done

##Bowtie index
if [ ! -e $workdir2/Node_min3ORF.contig.fa.id95_cov90.1.bt2 ]
then
bowtie2-build $workdir2/Node_min3ORF.contig.fa.id95_cov90 $workdir2/Node_min3ORF.contig.fa.id95_cov90
fi







#mapping

if [ ! -d ${workdir2}/Bowtie ]
then
mkdir ${workdir2}/Bowtie
fi

if [ ! -d ${workdir2}/Bowtie/ALL ]
then
mkdir ${workdir2}/Bowtie/ALL
fi

if [ ! -d ${workdir2}/Bowtie/Min3ORF ]
then
mkdir ${workdir2}/Bowtie/Min3ORF
fi

if [ ! -d ${workdir2}/Bowtie/ALL/sam ]
then
mkdir ${workdir2}/Bowtie/ALL/sam
fi

if [ ! -d ${workdir2}/Bowtie/Min3ORF/sam ]
then
mkdir ${workdir2}/Bowtie/Min3ORF/sam
fi


if [ ! -d ${workdir2}/Bowtie/ALL/Bowtie_resume ]
then
mkdir ${workdir2}/Bowtie/ALL/Bowtie_resume
fi

if [ ! -d ${workdir2}/Bowtie/Min3ORF/Bowtie_resume ]
then
mkdir ${workdir2}/Bowtie/Min3ORF/Bowtie_resume
fi


Liste_genome=$(ls $WorkDirBowtie| awk -F "." '{print $1}' | awk -F "_" '{print $1"_"$2"_"$3}'sort | uniq)
#CC1_SP30_S22 sous ce format

for genome in $Liste_genome

do

#sur le ALL contig

if [ ! -e ${workdir2}/Bowtie/ALL/sam/${genome}.sam ]
then
echo "je fais bowtie pour le ALL de ${genome}"
bowtie2 -p 10 -a -x ${workdir2}/ALL.contig.fa.id95_cov90 -q -1 ${WorkDirBowtie}/${genome}_R1.ctd.urqt.fastq -2 ${WorkDirBowtie}/${genome}_R2.ctd.urqt.fastq --local --sensitive-local -S ${workdir2}/Bowtie/ALL/sam/${genome}.sam > ${workdir2}/Bowtie/ALL/Bowtie_resume/${genome}.txt 2>&1
fi


#sur les contigsMin3ORF
if [ ! -e ${workdir2}/Bowtie/Min3ORF/sam/${genome}.sam ]
then
echo "je fais bowtie pour le min3ORF de ${genome}"
bowtie2 -p 10 -a -x ${workdir2}/Node_min3ORF.contig.fa.id95_cov90 -q -1 ${WorkDirBowtie}/${genome}_R1.ctd.urqt.fastq -2 ${WorkDirBowtie}/${genome}_R2.ctd.urqt.fastq --local --sensitive-local -S ${workdir2}/Bowtie/Min3ORF/sam/${genome}.sam > ${workdir2}/Bowtie/Min3ORF/Bowtie_resume/${genome}.txt 2>&1
fi
done


################################

#generate bam
if [ ! -d ${workdir2}/Bowtie/ALL/bam ]
then
mkdir ${workdir2}/Bowtie/ALL/bam
fi

if [ ! -d ${workdir2}/Bowtie/Min3ORF/bam ]
then
mkdir ${workdir2}/Bowtie/Min3ORF/bam
fi

sam_files_all=$(ls ${workdir}/Patient/ALL/Bowtie/ALL/sam/*.sam | awk -F "/" '{print $NF}') #AA1_SP30_S13.sam donne ce format la

for sam_all in $sam_files_all
do
  sam_file_name=${sam_all%*.*}
  echo $sam_file_name
  if [ ! -e ${workdir2}/Bowtie/ALL/bam/${sam_file_name}.bam ]
  then
  echo "je fais samtools pour le ALL de ${sam_file_name}.sam"
  samtools view -Shb ${workdir2}/Bowtie/ALL/sam/${sam_file_name}.sam > ${workdir2}/Bowtie/ALL/bam/${sam_file_name}.bam
  fi

done


sam_files_min3ORF=$(ls ${workdir2}/Bowtie/Min3ORF/sam/*.sam | awk -F "/" '{print $NF}')

for sam_min3ORF in $sam_files_min3ORF
do
  sam_file_name=${sam_min3ORF%*.*}
  if [ ! -e ${workdir2}/Bowtie/Min3ORF/bam/${sam_file_name}.bam ]
  then
  echo "je fais samtools pour le Min3ORF de ${sam_file_name}.sam"
  samtools view -Shb ${workdir2}/Bowtie/Min3ORF/sam/${sam_file_name}.sam > ${workdir2}/Bowtie/Min3ORF/bam/${sam_file_name}.bam
  fi

done


#faire le counting sur les point bam

if [ ! -d ${workdir2}/Bowtie/ALL/comptage ]
then
mkdir ${workdir2}/Bowtie/ALL/comptage
fi

if [ ! -d ${workdir2}/Bowtie/Min3ORF/comptage ]
then
mkdir ${workdir2}/Bowtie/Min3ORF/comptage
fi



# samtools

bam_files_all=$(ls ${workdir2}/Bowtie/ALL/sam/*.sam | awk -F "/" '{print $NF}')

for bam_all in $bam_files_all
do
  bam_file_name=${bam_all%*.*}
  if [ ! -e ${workdir2}/Bowtie/ALL/comptage/${bam_file_name}.txt ]
  then
  samtools sort -@ 4 ${workdir2}/Bowtie/ALL/bam/${bam_file_name}.bam -o ${workdir2}/Bowtie/ALL/bam/${bam_file_name}.sort.bam
  samtools index ${workdir2}/Bowtie/ALL/bam/${bam_file_name}.sort.bam
  samtools idxstats ${workdir2}/Bowtie/ALL/bam/${bam_file_name}.sort.bam > ${workdir2}/Bowtie/ALL/comptage/${bam_file_name}.txt
#awk '{print $1,$2,$3}' ${workdir2}/Bowtie/ALL/comptage/${bam_file_name}.txt | grep -v "*" > ${workdir2}/Bowtie/ALL/comptage/${bam_file_name}.format.txt
fi
done

bam_files_all=$(ls ${workdir2}/Bowtie/Min3ORF/sam/*.sam | awk -F "/" '{print $NF}')

for bam_all in $bam_files_all
do
  bam_file_name=${bam_all%*.*}
  if [ ! -e ${workdir2}/Bowtie/Min3ORF/comptage/${bam_file_name}.txt ]
  then
  samtools sort -@ 4 ${workdir2}/Bowtie/Min3ORF/bam/${bam_file_name}.bam -o ${workdir2}/Bowtie/Min3ORF/bam/${bam_file_name}.sort.bam
  samtools index ${workdir2}/Bowtie/Min3ORF/bam/${bam_file_name}.sort.bam
  samtools idxstats ${workdir2}/Bowtie/Min3ORF/bam/${bam_file_name}.sort.bam > ${workdir2}/Bowtie/Min3ORF/comptage/${bam_file_name}.txt
  #awk '{print $1,$2,$3}' ${workdir2}/Bowtie/Min3ORF/comptage/${bam_file_name}.txt | grep -v "*" > ${workdir2}/Bowtie/Min3ORF/comptage/${bam_file_name}.format.txt
fi
done






#count_matrix.py

if [ ! -e ${workdir2}/Bowtie/ALL/comptage/count_matrix_all.txt ]
then

/Users/camilledhumieres/Documents/Logiciels/MBMA/count_matrix.py -d ${workdir2}/Bowtie/ALL/comptage/ -o ${workdir2}/Bowtie/ALL/comptage/count_matrix_all.txt
fi

if [ ! -e ${workdir2}/Bowtie/Min3ORF/comptage/count_matrix_min3ORF.txt ]
then

/Users/camilledhumieres/Documents/Logiciels/MBMA/count_matrix.py -d ${workdir2}/Bowtie/Min3ORF/comptage/ -o ${workdir2}/Bowtie/Min3ORF/comptage/count_matrix_min3ORF.txt

fi


#creation de nouveau FOLDER et cp de la count_matrix


if [ ! -d ${workdir2}/Result/Count_Matrix ]
then
mkdir ${workdir2}/Result/Count_Matrix
fi



if [ ! -e ${workdir2}/Result/Count_Matrix/count_matrix_min3ORF.txt ]
then
cp ${workdir2}/Bowtie/Min3ORF/comptage/count_matrix_min3ORF.txt ${workdir2}/Result/Count_Matrix/
fi


if [ ! -e ${workdir2}/Result/Count_Matrix/count_matrix_all.txt ]
then
cp ${workdir2}/Bowtie/ALL/comptage/count_matrix_all.txt ${workdir2}/Result/Count_Matrix/
fi
