#!/bin/bash

#simplement changer Wordir et Filin et lancer Do_HMM.sh + penser a changer le chemin dans Integrase


# --out  pour avoir la sortie normal puis la gerer avec convert_HMM puis faire la sortie tablulé --tblout

# meilleur hit sur le score = colonne 6 de la sortie tabulée


WorkDir=$1
#ex"/Volumes/BIGDATA/Sequence_High_211.311.411.511/Read_Bruts/L1/processed/Patient/ALL"
Filin=$2
#ex Node_min3ORF.contig.fasta.id95_cov90.prt
Filin_Gen=$3
#ex Node_min3ORF.contig.fasta.gen
Filout=$WorkDir/Result


#je cree le dossier result si celui ci n'est pas créé

if [ ! -d  $WorkDir/Result ]
then
mkdir $WorkDir/Result
fi


# faire un HMM avec le 16s
# E value a 0.1
#ATTENTION probleme car le HMM 16S doit etre en nucleique et pas en proteique .... Revoir ligne de commande - je fais la recherche sur le fichier des genes. Camille le 10/07/18

if [ ! -e $Filout/${Filin_Gen}.HMM_16S.tab ]
then
echo "je fais HMM avec 16S sur le $Filin_Gen"
hmmsearch --tblout $Filout/${Filin_Gen}.HMM_16s.tab -o $Filout/${Filin_Gen}.HMM_16s.out  --cpu 6 -E 0.1 /Volumes/BIGDATA/DB/16S_HMM/16S.hmm3 $WorkDir/${Filin_Gen}
prot=$(awk 'NR>3 {print $1}' $Filout/${Filin_Gen}.HMM_16s.tab| sort -n | uniq)
for u in $prot
do
grep $u $Filout/${Filin_Gen}.HMM_16s.tab | sort -gk6 | tail -n 1 >> $Filout/${Filin_Gen}.HMM_16s.tab.best.res
done
fi


#HMM marqueur du monde bacterien + creation d'un fichier resume pour aider a la construction du tableau final

if [ ! -e $Filout/${Filin}.HMM_Marqueur_Bacterien.tab ]
then
    echo "je fais HMM avec marqueur du monde bacterien sur le $Filin"
    hmmsearch --tblout $Filout/${Filin}.HMM_Marqueur_Bacterien.tab -o $Filout/${Filin}.HMM_Marqueur_Bacterien.out  --cpu 6 --cut_ga /Volumes/BIGDATA/DB/conta_Marie_HMM/bacterial_128_PFAM.hmm  $WorkDir/${Filin}
    prot=$(awk 'NR>3 {print $1}' $Filout/${Filin}.HMM_Marqueur_Bacterien.tab| sort -n | uniq)
    for u in $prot
    do
    grep $u $Filout/${Filin}.HMM_Marqueur_Bacterien.tab | sort -gk6 | tail -n 1 >> $Filout/${Filin}.HMM_Marqueur_Bacterien.tab.best.res
    done
    awk -F"_" '{print $1"_"$2"_"$3"_"$4}' $Filout/${Filin}.HMM_Marqueur_Bacterien.tab.best.res | sort | uniq -c | awk '{print $2,$1}' > $Filout/${Filin}.HMM_Marqueur_Bacterien.tab.best.res.resume
fi


#HMMsearch (score > 40) avec DB « Pool_clusters_hallmark.hmm » (gène essentiel)


#if [ ! -e $Filout/${Filin}.Hallmark_HMM.tab ]
#then
#   echo "je fais HMM avec Hallmark_virsoter sur le $Filin"
#    hmmsearch --tblout $Filout/${Filin}.Hallmark_HMM.tab -o $Filout/${Filin}.Hallmark_HMM.out --cpu 6 -T 40 /Volumes/BIGDATA/DB/Virsorter-HMM-DB-Hallmark/Pool_clusters_hallmark.hmm  $WorkDir/${Filin}
#    prot=$(awk 'NR>3 {print $1}' $Filout/${Filin}.Hallmark_HMM.tab| sort -n | uniq)
#    for u in $prot
#    do
#    grep $u $Filout/${Filin}.Hallmark_HMM.tab | sort -gk6 | tail -n 1 >> $Filout/${Filin}.Hallmark_HMM.tab.best.res
#    done
#fi
	##recuperer la colonne 1 / enlever la redondance (nombre de prot qui match). Quel est le best hit pour le profil HMM? prendre le meilleur SCORE


#HMMsearch sur Phage_Finder --cut_ga


if [ ! -e $Filout/${Filin}.Phage_Finder.tab ]
then
    echo "je fais HMM Phage finder sur le $Filin"
    hmmsearch --tblout $Filout/${Filin}.Phage_Finder.tab  -o $Filout/${Filin}.Phage_Finder.out  --cpu 6 --cut_ga /Volumes/BIGDATA/DB/PhageFinder_HMM/BANK-PHAGEFINDER $WorkDir/${Filin}
    prot=$(awk 'NR>3 {print $1}' $Filout/${Filin}.Phage_Finder.tab | sort -n | uniq)
    for u in $prot
    do
    grep $u $Filout/${Filin}.Phage_Finder.tab | sort -gk6 | tail -n 1 >> $Filout/${Filin}.Phage_Finder.tab.best.res
    done
fi


#HMMsearch sur Integrase (serine et tyrosine) profiles (10^-3)
#-------------VERIFIER QUE SEARCH INTEGRASE MARCHE BIEN CAR ARGUMENTS NOUVEAUX-------------------------

if [ ! -e $Filout/${Filin}.integrase ]
then
  echo "recherche d'integrase sur le $Filin"
  /Users/camilledhumieres/Documents/Scripts/Script/Temoins/SearchIntegrase.sh $WorkDir $Filin

fi





#HMM PFAM
if [ ! -e $Filout/${Filin}.PFAM.tab ]
then
    echo "je fais HMM PFAM sur le $Filin"
    hmmsearch --tblout $Filout/${Filin}.PFAM.tab   -o $Filout/${Filin}.PFAM.out  --cpu 6 --cut_ga /Volumes/BIGDATA/DB/PFAM30.0/Pfam-A.hmm $WorkDir/${Filin}
    prot=$(awk 'NR>3 {print $1}' $Filout/${Filin}.PFAM.tab  | sort -n | uniq)
    for u in $prot
    do
    grep $u ${Filout}/${Filin}.PFAM.tab  | sort -gk6 | tail -n 1 >> ${Filout}/${Filin}.PFAM.tab.best.res
    done

fi




#HMM TIGRFAM
if [ ! -e $Filout/${Filin}.TIGRFAM.tab ]
then
    echo "je fais HMM TIGRFAM sur le $Filin"
    hmmsearch --tblout $Filout/${Filin}.TIGRFAM.tab  -o $Filout/${Filin}.TIGRFAM.out  --cpu 6 --cut_ga /Volumes/BIGDATA/DB/TIGRFAMs15.0/TIGRFAMs_15.0_ALL.HMM $WorkDir/${Filin}
    prot=$(awk 'NR>3 {print $1}' $Filout/${Filin}.TIGRFAM.tab | sort -n | uniq)
    for u in $prot
    do
    grep $u ${Filout}/${Filin}.TIGRFAM.tab | sort -gk6 | tail -n 1 >> ${Filout}/${Filin}.TIGRFAM.tab.best.res
    done

fi
