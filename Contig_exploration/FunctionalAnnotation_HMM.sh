#!/bin/tcsh
# 30 Novembre 2016
#modif le 26 Juillet 2017
#re modif le 25 Octobre 2017 pour faire le Do_Explo_Contig_CEREMI mais je ne touche pas a cette version, je vais en creer une autre dans le dosier CEREMI


#script pour aller echercher dans PFAM les prot spe de phages et faire un RESUME
#appelé du folder /Volumes/BIGDATA/DB/HMM_Functionnal la ligne de commande /Users/camilledhumieres/Documents/Scripts/Script/Temoins/FunctionalAnnotation_HMM.sh Node_min3ORF.contig.fa.prt


set DB =  /Volumes/BIGDATA/DB/HMM_Functionnal  #fichier avec le details fonctionnelle des profils HMM
#set HMMDB POUR MARIE = /Volumes/GEM/Human_Microbiome_Project/Viromes/HMMBank
set HMMDB = $1 #endroit avec ma sortie PFAM
#ex Volumes/BIGDATA/Echantillons_Temoins/Read_bruts/ALL_reads/processed/Result/

set Functions = `ls $DB/*hmm3.lst`
#echo $Functions
set Functions2 = `ls $DB/*hmm3.lst | awk -F "." '{print $1}'`
#echo $Functions2
set fileIN = $2 #name file = Node_min3ORF.contig.fa.prt"

set fileOUT = resultFunctANNot.lst

set fileOUT2 = resultFunctANNot.tab



if(! -e $HMMDB/$fileIN.PFAM.tab.best.res) then
	echo "problem : pfam and tigfam files don't exist"
	echo "check your file : $fileIN.proteins.min3ORF.pfam and  $fileIN.proteins.min3ORF.tigrfam"
endif



if(-e $HMMDB/$fileIN.PFAM.tab.best.res) then
	if(! -d  $HMMDB/$fileIN/) then
		mkdir $HMMDB/$fileIN/
	endif


	foreach MainFunction ($Functions)
   	echo $MainFunction

    	set MainFunction2 = `echo $MainFunction:t:r`
    	set profiles = `awk '{if(NR>1) print $1}' $MainFunction`
        echo $profiles
    	if(! -e $HMMDB/$fileIN/$fileIN.$MainFunction2) then
            touch $HMMDB/$fileIN/$fileIN.$MainFunction2
            foreach profile ($profiles)
                #set res = `egrep -h -c $profile $HMMDB/$fileIN*PFAM.tab | awk -F ":" '{print sum += $1}' | tail -n 1`
                #echo $profile $res
#if($res != 0) then
#echo $profile
                egrep -h $profile $HMMDB/$fileIN*PFAM.tab.best.res >> $HMMDB/$fileIN/$fileIN.$MainFunction2
                egrep -h $profile $HMMDB/$fileIN*TIGRFAM.tab.best.res >> $HMMDB/$fileIN/$fileIN.$MainFunction2
                echo "Contig_Name" $MainFunction2 > $HMMDB/$fileIN/$fileIN.$MainFunction2.resume

                awk -F"_" '{print $1"_"$2"_"$3"_"$4}' $HMMDB/$fileIN/$fileIN.$MainFunction2 | sort | uniq -c | awk '{print $2,$1}' >> $HMMDB/$fileIN/$fileIN.$MainFunction2.resume
            endif
        	end

    endif
end








#if(! -e $HMMDB/$fileIN/$fileOUT2) then
#echo "TypePhageome" $Functions2 >> $HMMDB/$fileIN/$fileOUT2
#endif

#echo -n $fileIN >> $HMMDB/$fileIN/$fileOUT2

#foreach MainFunction ($Functions)
#set MainFunction2 = `echo $MainFunction:r:r`
#    set Nprot = `awk '{print $1}'  $HMMDB/$fileIN/$fileIN.$MainFunction2 | sort -n | uniq`
#   echo $fileIN $MainFunction2 $#Nprot >> $HMMDB/$fileIN/$fileOUT

#   echo -n  " $#Nprot " >> $HMMDB/$fileIN/$fileOUT2

#end
#echo " " >> $HMMDB/$fileIN/$fileOUT2

#endif
