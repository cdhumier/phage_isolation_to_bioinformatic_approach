#!/bin/bash





################COMMAND LINE##########################
#./do_explo_contig path_to_each_sample
######################################################


WorkDir=$1


#DB
DB_NCBI=/Volumes/BIGDATA/DB/NCBI_NT/nt
GET_TAXO=get_taxonomy2.py
EXTRACT_NCBI=ExtractNCBIDB.py

Patients_ALL=$(ls $WorkDir)

for Patient in $Patients_ALL
do

	#variable et creation directory
	echo "je commence le do_explo_contig pour le patient $Patient"

	Filin_path_Contig=${WorkDir}/${Patient}/${Patient}.Node_min3ORF.contig.fasta
	Filin_Contig=$(echo ${Filin_path_Contig} | awk -F "/" '{print $NF}')

	Filin_HMM_path=${WorkDir}/${Patient}/${Patient}.Node_min3ORF.contig.fasta.prt
	Filin_prot=$(echo $Filin_HMM_path | awk -F "/" '{print $NF}')
	#Filin_genes=$(echo $Filin_HMM_path | awk -F "/" '{print $NF}' | awk -F "." '{print $1"."$2"."$3"."$4}')'.gen'

	Filin_NCBI_path=${WorkDir}/${Patient}/${Patient}.Node_min3ORF.contig.fasta.gen
	Filin_NCBI_name=$(echo $Filin_NCBI_path | awk -F "/" '{print $NF}')

	WorkDir_virsorter="/BIGDATA3/DB_phages_0218_pipeline_test/Patient"

	if [ ! -d ${WorkDir}/${Patient}/Result ]
	then
	mkdir ${WorkDir}/${Patient}/Result
	fi



	if [ ! -d ${WorkDir}/${Patient}/Result/Virsorter ]
	then
	mkdir ${WorkDir}/${Patient}/Result/Virsorter
	fi


	if [ ! -d ${WorkDir}/${Patient}/Result/PHACTS ]
	then
	mkdir ${WorkDir}/${Patient}/Result/PHACTS
	fi



	if [ ! -d ${WorkDir}/${Patient}/Result/PHACTS/Separate_contig ]
	then
	mkdir ${WorkDir}/${Patient}/Result/PHACTS/Separate_contig
	fi

	if [ ! -d ${WorkDir}/${Patient}/Result/PHACTS/Output ]
	then
	mkdir ${WorkDir}/${Patient}/Result/PHACTS/Output
	fi



	#Virsorter

	if [ ! -e ${WorkDir}/${Patient}/Result/Virsorter/${Filin_Contig} ]
	then
	echo "${Filin_path_Contig} ${WorkDir}/${Patient}/Result/Virsorter"
	cp ${Filin_path_Contig} ${WorkDir}/${Patient}/Result/Virsorter
	fi



	if [ ! -e ${WorkDir}/${Patient}/Result/Virsorter/VIRSorter_global-phage-signal.csv ]
	then

	echo "je commence Virsorter pour le patient $Patient"
	docker-machine start default

	eval "$(docker-machine env default)"

	#Juillet 18 j'ai change le chemin de virsorterdata pour que tout soit dans le BIGDATA3
	docker run -v /BIGDATA3/Echantillons_CEREMI/DB/VirsorterData:/data -v ${WorkDir_virsorter}/${Patient}/Result/Virsorter:/wdir -w /wdir --rm discoenv/virsorter:v1.0.3 --db 2 --fna /wdir/${Filin_Contig} --virome 1
	fi

	liste_fasta=$(ls ${WorkDir}/${Patient}/Result/Virsorter/Predicted_viral_sequences/*.fasta)

	output=${WorkDir}/${Patient}/Result/Virsorter/${Patient}.Node_min3ORF.contig_Virsorter_Result_db2_Format.txt

	if [ ! -e $output ]

	then

	echo Contig_Name Size Cat Circular > $output


	for f in $liste_fasta


	do

	grep ">" $f | tr -d ">" | tr -s "-" "_" | awk -F "_" '{if($NF<=3 && $9=="circular") print $2"_"$3"_"$4"_"$5,$6,$11,"YES"; if($NF<=3 && $9!="circular") print $2"_"$3"_"$4"_"$5,$6,$10,"NO"; if($NF>3 && $9=="circular") print $2"_"$3"_"$4"_"$5,$6,$17,"YES"; if($NF>3 && $9!="circular") print $2"_"$3"_"$4"_"$5,$6,$16,"NO"}' >> $output

	done


	fi

	#ALL_HMM

	echo "je commence HMM pour le patient $Patient"

	.Do_HMM.sh ${WorkDir}/${Patient} ${Filin_prot} ${Filin_NCBI_name}

	#GRR

	#cf wGRR part


	#Functionnal_annotation_HMM_Phage


	echo "je commence Functionnal_annotation_HMM_Phage pour le patient $Patient"

	.FunctionalAnnotation_HMM.sh ${WorkDir}/${Patient}/Result/ ${Filin_prot}


	#NCBI

	echo "je commence NCBI pour le patient $Patient"

	if [ ! -e ${WorkDir}/${Patient}/Result/${Filin_NCBI_name}.NCBI.NT.tab ]
	then




	#blastn
	blastn -query ${Filin_NCBI_path} -db ${DB_NCBI} -outfmt "6 qseqid sseqid qlen length mismatch gapopen qstart qend sstart send pident qcovs evalue bitscore" -evalue 0.00001 -max_target_seqs 10 -task megablast -out ${WorkDir}/${Patient}/Result/${Filin_NCBI_name}.NCBI.NT.tab -num_threads 6 2> ${WorkDir}/${Patient}/Result/${Filin_NCBI_name}.NCBI.NT.log
	fi


	if [ ! -e ${WorkDir}/${Patient}/Result/${Filin_NCBI_name}.NCBI.NT.tab.get_taxo ]
	then
	#get_taxonomy (HADRIEN G)
	source activate python3
	python $GET_TAXO -i ${WorkDir}/${Patient}/Result/${Filin_NCBI_name}.NCBI.NT.tab  -d /Volumes/BIGDATA/DB/NCBI_HadrienG/taxadb_full.sqlite  -t /Volumes/BIGDATA/DB/NCBI_NT/gi_taxid_nucl.dmp  -o ${WorkDir}/${Patient}/Result/${Filin_NCBI_name}.NCBI.NT.tab.get_taxo
	source deactivate python3
	fi


	#extract NCBI DB
	if [ ! -e ${WorkDir}/${Patient}/Result/${Filin_NCBI_name}.NCBI.NT.tab.get_taxo_cov90 ]
	then
	$EXTRACT_NCBI -f ${WorkDir}/${Patient}/Result/${Filin_NCBI_name}.NCBI.NT.tab -g ${WorkDir}/${Patient}/Result/${Filin_NCBI_name}.NCBI.NT.tab.get_taxo  -o ${WorkDir}/${Patient}/Result/${Filin_NCBI_name}.NCBI.NT.tab.get_taxo_cov90 -nb 1 -fc 90 2> ${WorkDir}/${Patient}/Result/${Filin_NCBI_name}.NCBI.NT.tab.get_taxo_cov90.log
	fi



	#prendre les 4 premieres colonnes car apres probleme formatage
	if [ ! -e ${WorkDir}/${Patient}/Result/${Filin_NCBI_name}.NCBI.NT.tab.get_taxo_cov90_4col ]
	then
	awk '{print $1,$2,$3,$4}' ${WorkDir}/${Patient}/Result/${Filin_NCBI_name}.NCBI.NT.tab.get_taxo_cov90 > ${WorkDir}/${Patient}/Result/${Filin_NCBI_name}.NCBI.NT.tab.get_taxo_cov90_4col

	fi
	#prendre 3 premieres colonnes en separant par \t
	if [ ! -e ${WorkDir}/${Patient}/Result/${Filin_NCBI_name}.NCBI.NT.tab.get_taxo_cov90_3col ]
	then
	awk -F "\t" '{print $1"\t"$2"\t"$3}' ${WorkDir}/${Patient}/Result/${Filin_NCBI_name}.NCBI.NT.tab.get_taxo_cov90 > ${WorkDir}/${Patient}/Result/${Filin_NCBI_name}.NCBI.NT.tab.get_taxo_cov90_3col

	fi

	if [ ! -e ${WorkDir}/${Patient}/Result/PHACTS/phacts_parsing_output_2SDvsMean.txt ]
	then

	#PHACTS
	echo "je commence PHACTS pour le patient $Patient"
	#separate contigs
	pythonw /Users/camilledhumieres/Documents/Scripts/Script/CEREMI/Separate_contig.py $Filin_HMM_path ${WorkDir}/${Patient}/Result/PHACTS/Separate_contig

	cp -r ${WorkDir}/${Patient}/Result/PHACTS/Separate_contig /Users/camilledhumieres/Documents/Logiciels/PHACTS/

	#go to the right place
	cd /Users/camilledhumieres/Documents/Logiciels/PHACTS/

	./RunContigsPHACTS_CEREMI.sh ${WorkDir}/${Patient}/Result/PHACTS/Output Separate_contig

	#rm
	rm -r /Users/camilledhumieres/Documents/Logiciels/PHACTS/Separate_contig

	#parsing output
	python /Users/camilledhumieres/Documents/Scripts/Script/CEREMI/ParsePhacts.py ${WorkDir}/${Patient}/Result/PHACTS/Output ${WorkDir}/${Patient}/Result/PHACTS/phacts_parsing_output_2SDvsMean.txt

	fi

done
