# coding: utf-8
import ipyparallel as parallel
import pandas as pd
import matplotlib.pyplot as plt
import subprocess
import networkx as nx
import time
from itertools import combinations, product
import numpy as np
import os
import pickle
import seaborn as sns
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
from Bio import SeqIO
import glob
import sys
import shlex
import shutil

#*******************************EX COMMAND LIGN*******************************************

#python Do_GRR_Contig_HMP.py filin.contig.prt

#*****************************************************************************************


#******************************************************************************************
#*********DEF******************************************************************************

def cat(list_files, output):
    """
    Concatenate all files in 'list_files' and save result in 'output'
    Concat using shutil.copyfileobj, in order to copy by chunks, to
    avoid memory problems if files are big.
    """
    with open(output, "w") as outf:
        for file in list_files:
            with open(file, "r") as inf:
                shutil.copyfileobj(inf, outf)


def decision(line, x, y):
        if (pd.isnull(line[x])) or (pd.isnull(line[y])):
            return np.nan
        elif line[x] == line[y]:
            return 1
        else:
            return 0



def GRR_Decison(df):
    df['Phage_Phylum_HOST_HMP_def']=df.apply(lambda x: x.Phylum if x.GRR >= (24.78*np.exp(-0.32*x.N_prot)+13.98) else "GRR_under_threshold", axis=1)








#*********ARGUMENTS***********************************************************************************

filin=sys.argv[-1] #filin est le chemin complet du fichier de contigs a opposé a phage_1116
#******************************************************************************************************

#*********---df-----***********************************************************************************

df_number = pd.read_table('{}.GRR.nbProt.lst'.format(filin), names=["N_prot","Name_ID"], sep=" ")
df_number.set_index("Name_ID", inplace=1)

df_details = pd.read_csv("/Volumes/BIGDATA/DB/HMP_Prophages/CORRESP_TABLE_HMUS.1215_format2_camille.txt", sep="\t")

#*******************************************************************************************************

if os.path.isfile(filin):
    print "ok input_file exists"
else:
    print "Input Contig file doesn't exist"



if os.path.isfile(filin+".phr"):
    print "filin already indexed for blast"
else:
    cmd="makeblastdb -in {} -dbtype 'prot'".format(filin)
    subprocess.call(shlex.split(cmd))


if not os.path.exists(os.path.join(os.path.dirname(filin), "Result")):
    os.makedirs(os.path.join(os.path.dirname(filin), "Result"))

if not os.path.exists(os.path.join(os.path.dirname(filin), "Result", "GRR")):
    os.makedirs(os.path.join(os.path.dirname(filin), "Result", "GRR"))

if not os.path.exists(os.path.join(os.path.dirname(filin), "Result", "GRR","HMP_Prophages")):
    os.makedirs(os.path.join(os.path.dirname(filin), "Result", "GRR","HMP_Prophages"))




path_res = os.path.join(os.path.dirname(filin), "Result", "GRR", "HMP_Prophages")
filout_blast = os.path.join(path_res, os.path.basename(filin)+"_vs_ProPhage_HMP.res.all")
path_res_tmp = os.path.join(os.path.dirname(filin), "Result", "GRR", "HMP_Prophages","tmp")

if os.path.isfile(filout_blast):
    print "filout_blast already exits"
else:

    print "debut de Blast"



    db="/Volumes/BIGDATA/DB/HMP_Prophages/blast_format/HMP_prophage_allprt_format.prt"
    query=filin


    filout1 = os.path.join(path_res, os.path.basename(filin)+"Blast1.res")


    print "blast1"

    blastall_cmd1 = ["blastp", "-num_threads", "6","-query",
    query,"-outfmt", "6","-evalue", "1e-5", "-num_alignments",
    "2000","-db", db,"-out", filout1]
    subprocess.call(blastall_cmd1)


    filout2 = os.path.join(path_res, os.path.basename(filin)+"Blast2.res")

    print "blast2"


    blastall_cmd2 = ["blastp", "-num_threads", "6","-query",
    db,"-outfmt", "6","-evalue", "1e-5", "-num_alignments",
    "2000","-db", query,"-out", filout2]

    subprocess.call(blastall_cmd2)

    with open (filout_blast, "w") as fout:
        for F in glob.glob(os.path.join(path_res,"*res")):
            with open (F,"r") as fin:
                for line in fin:
                    fout.write(line)



if os.path.isfile("{}.df2_sorted".format(os.path.join(path_res, os.path.basename(filin)))):
    print "df2_sorted already exits"
else:

    df_blast=pd.read_table(filout_blast, header=None, usecols=[0, 1, 2, 10],
                        names=["element_ID_1", "element_ID_2", "perId", "evalue"])

    df_blast_tmp = df_blast.sort_values("perId").drop_duplicates(subset=["element_ID_1", "element_ID_2"]).sort_values(["element_ID_1", "element_ID_2"])

    df_blast_tmp["Name_ID_1"]=df_blast_tmp.element_ID_1.apply(lambda x: "_".join(x.split("_")[:-1]) if "HMUS" not in x else x[0:24])

    df_blast_tmp["Name_ID_2"]=df_blast_tmp.element_ID_2.apply(lambda x: x[0:24] if "HMUS" in x else "_".join(x.split("_")[:-1]))





    df1 = (df_blast_tmp.merge(df_number, left_on="Name_ID_1", right_index=True, )
                   .merge(df_number, left_on="Name_ID_2", right_index=True, suffixes=("_1","_2")))

    df1.sort_values(["element_ID_1", "element_ID_2"], inplace=1)

    df2 = df1[df1.Name_ID_1!=df1.Name_ID_2]

    df2["Name_group"] = df2.apply(lambda x: "{}_{}".format(*sorted([x["Name_ID_1"], x["Name_ID_2"]])), axis=1)

    df2.sort_values("Name_group", inplace=True)

    df2.to_csv("{}.df2_sorted".format(os.path.join(path_res, os.path.basename(filin))), index=False)


#print path_res

#do BBH
Result_bbh = os.path.join(path_res,"PostBBH.txt")

if os.path.isfile(Result_bbh):
    print "BBH already done "
else:
    print "je commence bbh pr HMP"

    cmd2="python /Users/camilledhumieres/Documents/Scripts/Script/Temoins/Do_BBH_BigTable_multi.py -in {}.df2_sorted -t 6 -w {}".format(os.path.join(path_res, os.path.basename(filin)), path_res)

    subprocess.call(shlex.split(cmd2))



    cat(glob.glob(os.path.join(path_res,"tmp","*")), Result_bbh)


if os.path.isfile("{}.df_grr".format(os.path.join(path_res, os.path.basename(filin)))):
    print "df_grr already exits grr ok"
else:
    print "je commence grr"

    df_bbh_all_tmp = pd.read_table(Result_bbh,
    header=None,
    names=["element_ID_2","element_ID_1","Name_ID_1","Name_ID_2","perId"])





    df_bbh_all = (df_bbh_all_tmp.merge(df_number, left_on="Name_ID_1", right_index=True, )
    .merge(df_number, left_on="Name_ID_2", right_index=True, suffixes=("_1","_2")))


    df_bbh_all = df_bbh_all.loc[:,~df_bbh_all.columns.duplicated()][["element_ID_1", "element_ID_2", "Name_ID_1",
    "Name_ID_2", "perId", "N_prot_1", "N_prot_2",]].sort_values(["Name_ID_1", "Name_ID_2"])

    _GRR = lambda x: sum(x["perId"]) / min(np.mean(x["N_prot_1"]), np.mean(x["N_prot_2"]))


    df_grr = df_bbh_all.groupby(["Name_ID_1", "Name_ID_2"]).apply(_GRR)

    df_grr = df_grr.to_frame(name="GRR").sort_index().reset_index()

    df_grr.to_csv('{}.df_grr'.format(os.path.join(path_res, os.path.basename(filin))), index=False)




if os.path.isfile("{}.df_grr_decision".format(os.path.join(path_res, os.path.basename(filin)))):
    print "df_grr_decision already exits"
else:

    df_grr=pd.read_csv('{}.df_grr'.format(os.path.join(path_res, os.path.basename(filin))))



    df_grr["Name_ID_1_order"]=df_grr.apply(lambda x: x.Name_ID_1 if "HMUS" not in x.Name_ID_1 else x.Name_ID_2, axis=1 )

    df_grr["Name_ID_2_order"]=df_grr.apply(lambda x: x.Name_ID_2 if "HMUS" in x.Name_ID_2 else x.Name_ID_1, axis=1 )




    df_grr=df_grr[df_grr.Name_ID_1_order != df_grr.Name_ID_2_order ]

    #Do Best GRR




    df_grr_best = df_grr.iloc[df_grr.groupby('Name_ID_1_order')["GRR"].idxmax().values][['Name_ID_1_order','Name_ID_2_order','GRR']]

    df_grr_best["Name_ID_2_format"]=df_grr_best.Name_ID_2_order.apply(lambda x: ".".join(x.split(".")[0:3]))

	#je rajoute la colonne avec le N_prot des contigs
    df_number.reset_index(inplace=1)
    df_grr_best_Nprot = df_grr_best.merge(df_number, left_on='Name_ID_1_order', right_on="Name_ID")[['Name_ID_1_order','Name_ID_2_format','GRR','N_prot']]

    df_grr_best_details = df_grr_best_Nprot.merge(df_details, how="left", left_on="Name_ID_2_format", right_on="ID")[['Name_ID_1_order', 'Name_ID_2_format', "GRR", "N_prot", "Phylum"]]



    GRR_Decison(df_grr_best_details)



    df_grr_best_details.to_csv("{}.df_grr_decision".format(os.path.join(path_res, os.path.basename(filin))))


    shutil.rmtree(path_res_tmp)
