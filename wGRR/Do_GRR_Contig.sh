#!/bin/bash



#Main script main script that calls 3 secondary scripts (Do_GRR_Contig_Phage1116.py, Do_GRR_Contig_HMP.py, Do_GRR_Contig_Microbial1116.py)

#*******************************EX COMMAND LIGN*******************************************

#./Do_GRR_Contig.sh filin.contig.prt

#******************************************************************************************


#creation of lst file

filin=$1

if [ ! -e $filin.GRR.nbProt.lst ]
echo "File with proteins details number already exists"
then

grep ">" $filin | tr -d ">" | awk -F "_" '{print $1"_"$2"_"$3"_"$4}' | sort | uniq -c | awk '{print $1,$2}' > $filin.GRR.nbProt.lst
grep ">" /Volumes/BIGDATA/DB/Phage_1116/alltogether.prot  | tr -d ">" | awk -F "." '{print $1}' | sort | uniq -c | awk '{print $1,$2}' >> $filin.GRR.nbProt.lst
grep ">" /Volumes/BIGDATA/DB/HMP_Prophages/blast_format/HMP_prophage_allprt_format.prt   | tr -d ">" | awk -F "_" '{print $1}' | sort | uniq -c | awk '{print $1,$2}' >> $filin.GRR.nbProt.lst
grep ">" /Volumes/BIGDATA/GRR_cutoff_test/Microbial1116_Prophages/ALL/alltogether_postnx0.95.prot | tr -d ">" | awk '{print $1}' | awk -F "_" '{print $1"."$3}'|  sort | uniq -c | awk '{print $1,$2}' >> $filin.GRR.nbProt.lst



fi



echo "Do_GRR_Contig_Phage1116.py"
pythonw /Users/camilledhumieres/Documents/Scripts/Script/GRR/Do_GRR_Contig_Phage1116.py $filin

echo "Do_GRR_Contig_HMP.py"
pythonw /Users/camilledhumieres/Documents/Scripts/Script/GRR/Do_GRR_Contig_HMP.py $filin

echo "Do_GRR_Contig_Microbial1116.py"
pythonw /Users/camilledhumieres/Documents/Scripts/Script/GRR/Do_GRR_Contig_Microbial1116.py $filin
